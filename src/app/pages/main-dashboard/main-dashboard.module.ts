import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {MainDashboardComponent  } from "./main-dashboard.component";

@NgModule({
  declarations: [MainDashboardComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: MainDashboardComponent,
      },
    ]),
  ]
})
export class MainDashboardModule { }
